import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:task/core/exception/api_exceptions.dart';
import 'package:task/core/helper/check_connection_helper.dart';
import 'package:task/core/helper/exception_helper.dart';
import 'package:task/core/interfaces/i_api_manager.dart';

class APIManager implements IAPIManager {
  final IExceptionHelper _exceptionHelper;
  final ICheckConnectionHelper _connectionHelper;

  APIManager(this._exceptionHelper, this._connectionHelper);

  @override
  Future<dynamic> getAPICall(String uri) async {
    try {
      await _connectionHelper.awaitConnection();
      final response = await get(Uri.parse(uri));
      return _response(response);
    } on TimeoutException {
      final e = TimeIsOutException();
      _exceptionHelper.send(e);
      throw e;
    } catch (e) {
      final e = FetchDataException();
      _exceptionHelper.send(e);
      throw e;
    }
  }

  dynamic _response(Response response) {
    switch (response.statusCode) {
      case 200:
        return jsonDecode(response.body);

      case 400:
        final e = BadRequestException();
        _exceptionHelper.send(e);
        throw e;

      case 404:
        final e = NotFoundException();
        _exceptionHelper.send(e);
        throw e;

      case 401:
      case 403:
        final e = UnauthorisedException();
        _exceptionHelper.send(e);
        throw e;

      default:
        final e = FetchDataException();
        _exceptionHelper.send(e);
        throw e;
    }
  }
}
