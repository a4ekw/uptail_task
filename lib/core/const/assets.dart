class Assets {
  static const String _root = 'assets';

  static String get translationJsonPath => '$_root/json/translation.json';
}
