import 'package:task/core/exception/error_keys.dart';
import 'package:task/core/interfaces/i_exception.dart';

class TimeIsOutException implements Exception, IException {
  @override
  final ErrorKeys errorKey = ErrorKeys.timeout;
}

class ConnectedException implements Exception, IException {
  @override
  final ErrorKeys errorKey = ErrorKeys.connected;
}

class NoConnectionException implements Exception, IException {
  @override
  final ErrorKeys errorKey = ErrorKeys.noConn;
}

class BadRequestException implements Exception, IException {
  @override
  final ErrorKeys errorKey = ErrorKeys.badRequest;
}

class NotFoundException implements Exception, IException {
  @override
  final ErrorKeys errorKey = ErrorKeys.notFound;
}

class UnauthorisedException implements Exception, IException {
  @override
  final ErrorKeys errorKey = ErrorKeys.denied;
}

class FetchDataException implements Exception, IException {
  @override
  final ErrorKeys errorKey = ErrorKeys.unknown;
}
