import 'package:task/core/exception/error_keys.dart';
import 'package:task/core/interfaces/i_exception.dart';

class CorruptedDataException implements Exception, IException {
  @override
  final ErrorKeys errorKey;

  CorruptedDataException(this.errorKey);
}
