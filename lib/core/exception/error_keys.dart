import 'package:get/get.dart';

enum ErrorKeys {
  dataCorrupted,
  dataPartiallyCorrupted,
  unknown,
  badRequest,
  denied,
  notFound,
  connected,
  noConn,
  timeout,
}

extension ExtErrorKeys on ErrorKeys {
  String get key {
    switch (this) {
      case ErrorKeys.dataCorrupted:
        return 'D_0'.tr;
      case ErrorKeys.dataPartiallyCorrupted:
        return 'D_1'.tr;
      case ErrorKeys.badRequest:
        return 'A_400'.tr;
      case ErrorKeys.denied:
        return 'A_401_403'.tr;
      case ErrorKeys.notFound:
        return 'A_404'.tr;
      case ErrorKeys.connected:
        return '';
      case ErrorKeys.noConn:
        return 'A_503'.tr;
      case ErrorKeys.timeout:
        return 'A_504'.tr;
      default:
        return 'U_EX'.tr;
    }
  }
}
