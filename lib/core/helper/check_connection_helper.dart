import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:task/core/exception/api_exceptions.dart';
import 'package:task/core/helper/exception_helper.dart';

abstract class ICheckConnectionHelper {
  void init();

  Future<bool> isConnected();

  Future<void> awaitConnection();
}

class CheckConnectionHelper implements ICheckConnectionHelper {
  final IExceptionHelper _exceptionHelper;

  CheckConnectionHelper(this._exceptionHelper);

  final _checker = InternetConnectionChecker.createInstance(
    checkTimeout: const Duration(seconds: 3),
    checkInterval: const Duration(seconds: 3),
  );

  @override
  void init() {
    _checker.onStatusChange.listen((state) {
      if (state == InternetConnectionStatus.connected) {
        _exceptionHelper.send(ConnectedException());
      } else {
        _exceptionHelper.send(NoConnectionException());
      }
    });
  }

  @override
  Future<bool> isConnected() async => await _checker.hasConnection;

  @override
  Future<void> awaitConnection() async {
    while (!(await isConnected())) {
      await Future.delayed(const Duration(seconds: 3));
    }
  }
}
