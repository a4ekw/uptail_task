import 'package:flutter/cupertino.dart';
import 'package:task/core/exception/error_keys.dart';
import 'package:task/core/exception/api_exceptions.dart';
import 'package:task/core/helper/exception_helper.dart';
import 'package:task/core/interfaces/i_exception.dart';
import 'package:task/presentation/dialogs/show_on_error_dialog.dart';

abstract class IErrorAlertHelper {}

class ErrorAlertHelper implements IErrorAlertHelper {
  final IExceptionHelper _exceptionHelper;

  ErrorAlertHelper(this._exceptionHelper);

  void init() => _exceptionHelper.stream.listen(_onListen);

  final _nonDismissibleAlertsSet = <ErrorKeys, GlobalKey<NavigatorState>>{};

  void _onListen(IException exception) {
    switch (exception.runtimeType) {
      case NoConnectionException:
        _show(
          exception.errorKey,
          exception.errorKey.key,
        );
        break;

      case ConnectedException:
        _close(ErrorKeys.noConn);
        break;

      default:
        showOnErrorDialog(
          msg: exception.errorKey.key,
          barrierDismissible: true,
        );
    }
  }

  void _show(ErrorKeys errorKey, msg) {
    if (!_nonDismissibleAlertsSet.keys.contains(errorKey)) {
      final key = GlobalKey<NavigatorState>();
      _nonDismissibleAlertsSet.addAll({errorKey: key});
      showOnErrorDialog(msg: msg, key: key, barrierDismissible: false);
    }
  }

  void _close(ErrorKeys errorKey) {
    final key = _nonDismissibleAlertsSet[errorKey];
    if (key?.currentContext != null) {
      _nonDismissibleAlertsSet.remove(errorKey);
      Navigator.of(key!.currentContext!).pop();
    }
  }
}
