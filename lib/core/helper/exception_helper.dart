import 'package:rxdart/rxdart.dart';
import 'package:task/core/interfaces/i_exception.dart';

abstract class IExceptionHelper {
  Stream<IException> get stream;

  void send(IException e);
}

class ExceptionHelper implements IExceptionHelper {
  final _controller = BehaviorSubject<IException>();

  @override
  Stream<IException> get stream => _controller.stream;

  @override
  void send(IException e) => _controller.add(e);
}
