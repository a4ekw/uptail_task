import 'dart:async';

abstract class IAPIManager {
  Future<dynamic> getAPICall(String uri);
}
