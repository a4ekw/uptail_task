import 'package:task/core/exception/error_keys.dart';

abstract class IException {
  ErrorKeys get errorKey;
}
