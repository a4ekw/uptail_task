abstract class IMapper<InputT, OutputT> {
  OutputT call(InputT obj);
}
