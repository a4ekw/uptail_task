abstract class IUseCase<Input, Output> {
  Output execute(Input input);
}
