abstract class IUseCaseNoParams<Output> {
  Output execute();
}
