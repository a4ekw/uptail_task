import 'package:get/get.dart';

class Languages extends Translations {
  final Map<String, Map<String, String>> _map = {};

  void add(Map<String, Map<String, String>> map) => _map.addAll(map);

  @override
  Map<String, Map<String, String>> get keys => _map;
}
