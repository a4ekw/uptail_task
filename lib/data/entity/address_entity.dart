import 'package:task/data/entity/geo_entity.dart';

class AddressEntity {
  late final String street;
  late final String suite;
  late final String city;
  late final String zipcode;
  late final GeoEntity geo;

  AddressEntity({
    required this.street,
    required this.suite,
    required this.city,
    required this.zipcode,
    required this.geo,
  });

  AddressEntity.fromJson(Map<String, dynamic> json) {
    street = json['street'] ?? 'NaN';
    suite = json['suite'] ?? 'NaN';
    city = json['city'] ?? 'NaN';
    zipcode = json['zipcode'] ?? 'NaN';
    geo = GeoEntity.fromJson(json['geo'] ?? {});
  }
}
