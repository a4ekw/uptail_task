class CompanyEntity {
  late final String name;
  late final String catchPhrase;
  late final String bs;

  CompanyEntity({required this.name, required this.catchPhrase, required this.bs});

  CompanyEntity.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? 'NaN';
    catchPhrase = json['catchPhrase'] ?? 'NaN';
    bs = json['bs'] ?? 'NaN';
  }
}
