class GeoEntity {
  late final String lat;
  late final String lng;

  GeoEntity({required this.lat, required this.lng});

  GeoEntity.fromJson(Map<String, dynamic> json) {
    lat = json['lat'] ?? 'NaN';
    lng = json['lng'] ?? 'NaN';
  }
}
