import 'package:task/data/entity/address_entity.dart';
import 'package:task/data/entity/company_entity.dart';

class UserEntity {
  late final int id;
  late final String name;
  late final String username;
  late final String email;
  late final AddressEntity address;
  late final String phone;
  late final String website;
  late final CompanyEntity company;

  UserEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? 'NaN';
    name = json['name'] ?? 'NaN';
    username = json['username'] ?? 'NaN';
    email = json['email'] ?? 'NaN';
    address = AddressEntity.fromJson(json['address'] ?? {});
    phone = json['phone'] ?? 'NaN';
    website = json['website'] ?? 'NaN';
    company = CompanyEntity.fromJson(json['company'] ?? {});
  }
}
