import 'package:get/get.dart';
import 'package:task/core/interfaces/i_mapper.dart';
import 'package:task/data/entity/address_entity.dart';
import 'package:task/domain/model/address_model.dart';

import 'geo_entity_mapper.dart';

class AddressEntityMapper implements IMapper<AddressEntity, AddressModel> {
  @override
  AddressModel call(AddressEntity obj) => AddressModel(
        street: obj.street,
        suite: obj.suite,
        city: obj.city,
        zipcode: obj.zipcode,
        geo: Get.find<GeoEntityMapper>().call(obj.geo),
      );
}
