import 'package:task/core/interfaces/i_mapper.dart';
import 'package:task/data/entity/company_entity.dart';
import 'package:task/domain/model/company_model.dart';

class CompanyEntityMapper implements IMapper<CompanyEntity, CompanyModel> {
  @override
  CompanyModel call(CompanyEntity obj) => CompanyModel(
        name: obj.name,
        catchPhrase: obj.catchPhrase,
        bs: obj.bs,
      );
}
