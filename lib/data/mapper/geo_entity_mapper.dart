import 'package:task/core/interfaces/i_mapper.dart';
import 'package:task/data/entity/geo_entity.dart';
import 'package:task/domain/model/geo_model.dart';

class GeoEntityMapper implements IMapper<GeoEntity, GeoModel> {
  @override
  GeoModel call(GeoEntity obj) => GeoModel(lat: obj.lat, lng: obj.lng);
}
