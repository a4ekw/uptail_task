import 'package:get/get.dart';
import 'package:task/core/interfaces/i_mapper.dart';
import 'package:task/data/entity/user_entity.dart';
import 'package:task/data/mapper/address_entity_mapper.dart';
import 'package:task/data/mapper/company_entity_mapper.dart';
import 'package:task/domain/model/user_model.dart';

class UserEntityMapper implements IMapper<UserEntity, UserModel> {
  @override
  UserModel call(UserEntity obj) => UserModel(
        id: obj.id,
        name: obj.name,
        username: obj.username,
        email: obj.email,
        address: Get.find<AddressEntityMapper>().call(obj.address),
        phone: obj.phone,
        website: obj.website,
        company: Get.find<CompanyEntityMapper>().call(obj.company),
      );
}
