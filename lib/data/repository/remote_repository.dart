import 'package:task/core/exception/error_keys.dart';
import 'package:task/core/exception/common_exceptions.dart';
import 'package:task/core/helper/exception_helper.dart';
import 'package:task/core/interfaces/i_api_manager.dart';
import 'package:task/data/entity/user_entity.dart';
import 'package:task/domain/repository/i_remote_repository.dart';
import 'package:task/environment.dart';

class RemoteRepository implements IRemoteRepository {
  final IAPIManager _manager;
  final IExceptionHelper _exceptionHelper;

  RemoteRepository(this._manager, this._exceptionHelper);

  @override
  Future<List<UserEntity>> fetchUsers() async {
    final json = await _manager.getAPICall(Environment.getUsersUrl);
    final users = <UserEntity>[];

    for (var item in json ?? []) {
      try {
        users.add(UserEntity.fromJson(item));
      } catch (_) {}
    }

    if (users.isEmpty) {
      _exceptionHelper.send(CorruptedDataException(ErrorKeys.dataCorrupted));
    } else if ((json.length) > users.length) {
      _exceptionHelper.send(CorruptedDataException(ErrorKeys.dataPartiallyCorrupted));
    }

    return users;
  }
}
