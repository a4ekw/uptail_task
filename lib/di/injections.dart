import 'package:get/get.dart';
import 'package:task/core/api/api_manager.dart';
import 'package:task/core/helper/check_connection_helper.dart';
import 'package:task/core/helper/error_alert_helper.dart';
import 'package:task/core/helper/exception_helper.dart';
import 'package:task/core/interfaces/i_api_manager.dart';
import 'package:task/core/translation/languages.dart';
import 'package:task/data/mapper/address_entity_mapper.dart';
import 'package:task/data/mapper/company_entity_mapper.dart';
import 'package:task/data/mapper/geo_entity_mapper.dart';
import 'package:task/data/mapper/user_entity_mapper.dart';
import 'package:task/data/repository/remote_repository.dart';
import 'package:task/domain/repository/i_remote_repository.dart';
import 'package:task/domain/use_case/convert_user_entity_to_model_use_case.dart';
import 'package:task/domain/use_case/fetch_translations_use_case.dart';
import 'package:task/domain/use_case/fetch_users_use_case.dart';
import 'package:task/domain/use_case/get_local_json_use_case.dart';
import 'package:task/presentation/scanner/bloc/scanner_bloc.dart';
import 'package:task/presentation/users/bloc/users_bloc.dart';

class Injections {
  static Future<void> init() async {
    /// Use Case

    Get.put(GetLocalJsonUseCase());

    /// Translations

    Get.put(Languages());

    /// Use Case

    final fetchTranslationsUseCase = FetchTranslationsUseCase(Get.find(), Get.find());
    await fetchTranslationsUseCase.execute();
    Get.put(fetchTranslationsUseCase);

    /// Helper

    Get.lazyPut<IExceptionHelper>(() => ExceptionHelper());

    Get.put<IErrorAlertHelper>(ErrorAlertHelper(Get.find())..init());

    Get.put<ICheckConnectionHelper>(CheckConnectionHelper(Get.find())..init());

    /// API Manager

    Get.lazyPut(() => APIManager(Get.find(), Get.find()));

    /// Repository

    Get.lazyPut<IRemoteRepository>(() => RemoteRepository(Get.find(), Get.find()));

    /// BloC

    Get.lazyPut<ScannerBloc>(() => ScannerBloc());

    /// Mapper

    Get.lazyPut(() => UserEntityMapper());

    Get.lazyPut(() => AddressEntityMapper());

    Get.lazyPut(() => CompanyEntityMapper());

    Get.lazyPut(() => GeoEntityMapper());

    /// Use Case

    Get.lazyPut(() => ConvertUserEntityToModelUseCase(Get.find()));

    Get.lazyPut(() => FetchUsersUseCase(Get.find(), Get.find()));

    /// BloC

    Get.lazyPut<UsersBloc>(() => UsersBloc(Get.find()));
  }
}
