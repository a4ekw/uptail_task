import 'package:task/domain/model/geo_model.dart';

class AddressModel {
  late final String street;
  late final String suite;
  late final String city;
  late final String zipcode;
  late final GeoModel geo;

  AddressModel({
    required this.street,
    required this.suite,
    required this.city,
    required this.zipcode,
    required this.geo,
  });

  @override
  String toString() => 'Address: $street, $city, $zipcode';
}
