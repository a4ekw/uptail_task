class CompanyModel {
  late final String name;
  late final String catchPhrase;
  late final String bs;

  CompanyModel({required this.name, required this.catchPhrase, required this.bs});

  @override
  String toString() => 'Company: $name, $catchPhrase, $bs';
}
