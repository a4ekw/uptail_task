class GeoModel {
  String lat;
  String lng;

  GeoModel({required this.lat, required this.lng});

  @override
  String toString() => 'Geo: $lat, $lng';
}
