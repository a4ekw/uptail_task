import 'package:task/domain/model/address_model.dart';
import 'package:task/domain/model/company_model.dart';

class UserModel {
  late final int id;
  late final String name;
  late final String username;
  late final String email;
  late final AddressModel? address;
  late final String phone;
  late final String website;
  late final CompanyModel company;

  UserModel({
    required this.id,
    required this.name,
    required this.username,
    required this.email,
    required this.address,
    required this.phone,
    required this.website,
    required this.company,
  });

  @override
  String toString() => 'User: $name, $username,\n$email,\n$phone,\n$address,\n$company';
}
