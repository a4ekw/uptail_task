

import 'package:task/data/entity/user_entity.dart';

abstract class IRemoteRepository {
  Future<List<UserEntity>> fetchUsers();
}
