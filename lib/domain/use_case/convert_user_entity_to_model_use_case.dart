import 'package:task/core/interfaces/i_use_case.dart';
import 'package:task/data/entity/user_entity.dart';
import 'package:task/data/mapper/user_entity_mapper.dart';
import 'package:task/domain/model/user_model.dart';

class ConvertUserEntityToModelUseCase implements IUseCase<UserEntity, UserModel> {
  ConvertUserEntityToModelUseCase(this._mapper);

  final UserEntityMapper _mapper;

  @override
  UserModel execute(UserEntity entity) => _mapper.call(entity);
}
