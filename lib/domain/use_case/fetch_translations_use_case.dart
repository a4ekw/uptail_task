import 'package:task/core/const/assets.dart';
import 'package:task/core/interfaces/i_use_case_no_params.dart';
import 'package:task/core/translation/languages.dart';
import 'package:task/domain/use_case/get_local_json_use_case.dart';

class FetchTranslationsUseCase implements IUseCaseNoParams<Future<void>> {
  final GetLocalJsonUseCase _useCase;
  final Languages _languages;

  FetchTranslationsUseCase(this._useCase, this._languages);

  @override
  Future<void> execute() async {
    try {
      final data = await _useCase.execute(Assets.translationJsonPath);
      _languages.add(
        (data as Map<String, dynamic>).map(
          (key, value) => MapEntry(key, (value as Map<String, dynamic>).cast<String, String>()),
        ),
      );
    } catch (_) {}
  }
}
