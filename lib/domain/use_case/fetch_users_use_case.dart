import 'package:task/core/interfaces/i_use_case_no_params.dart';
import 'package:task/data/entity/user_entity.dart';
import 'package:task/data/mapper/user_entity_mapper.dart';
import 'package:task/domain/model/user_model.dart';
import 'package:task/domain/repository/i_remote_repository.dart';

class FetchUsersUseCase implements IUseCaseNoParams<Future<List<UserModel>>> {
  final IRemoteRepository _repository;
  final UserEntityMapper _mapper;

  FetchUsersUseCase(this._repository, this._mapper);

  @override
  Future<List<UserModel>> execute() async {
    final list = await _repository.fetchUsers();
    final users = <UserModel>[];
    for (UserEntity entity in list) {
      users.add(_mapper.call(entity));
    }
    return users;
  }
}
