import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:task/core/interfaces/i_use_case.dart';

class GetLocalJsonUseCase implements IUseCase<String, Future<dynamic>> {
  @override
  Future<dynamic> execute(String path) async =>
      rootBundle.loadString(path).then((jsonStr) => jsonDecode(jsonStr));
}
