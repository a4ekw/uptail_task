class Environment {
  static String get baseUrl => 'https://jsonplaceholder.typicode.com';

  static String get getUsersUrl => '$baseUrl/users';
}
