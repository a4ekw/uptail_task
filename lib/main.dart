import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:task/core/translation/languages.dart';
import 'package:task/di/injections.dart';
import 'package:task/presentation/home_page.dart';
import 'package:task/presentation/scanner/bloc/scanner_bloc.dart';
import 'package:task/presentation/users/bloc/users_bloc.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Injections.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ScannerBloc>(create: (_) => Get.find()),
        BlocProvider<UsersBloc>(create: (_) => Get.find()),
      ],
      child: GetMaterialApp(
        translations: Get.find<Languages>(),
        locale: Get.deviceLocale,
        fallbackLocale: const Locale('en', 'US'),
        supportedLocales: const [
          // Locale('ru', 'RU'),
          Locale('en', 'US'),
        ],
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        home: const HomePage(),
      ),
    );
  }
}
