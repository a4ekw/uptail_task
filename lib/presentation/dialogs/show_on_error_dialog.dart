import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showOnErrorDialog({
  required String msg,
  GlobalKey? key,
  bool barrierDismissible = true,
}) {
  showDialog(
    barrierDismissible: barrierDismissible,
    context: Get.context!,
    builder: (BuildContext context) => AlertDialog(
      key: key,
      title: Center(
        child: Text(
          msg,
          textAlign: TextAlign.center,
        ),
      ),
      content: CircleAvatar(
        backgroundColor: Colors.black12,
        child: barrierDismissible
            ? IconButton(
                onPressed: Navigator.of(context).pop,
                icon: const Icon(Icons.close),
              )
            : const CircularProgressIndicator(),
      ),
    ),
  );
}
