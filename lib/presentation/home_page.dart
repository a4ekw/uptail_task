import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task/core/exception/api_exceptions.dart';
import 'package:task/core/exception/common_exceptions.dart';
import 'package:task/core/exception/error_keys.dart';
import 'package:task/core/helper/exception_helper.dart';
import 'package:task/presentation/scanner/scanner_page.dart';
import 'package:task/presentation/widget/custom_app_bar_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final useCase = Get.find<IExceptionHelper>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: 'scanner'.tr,
        leading: const SizedBox.shrink(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => const ScannerPage(),
          ),
        ),
        child: const Icon(Icons.qr_code_scanner),
      ), //
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 30.0),
            child: Text(
              'Error simulation',
              style: TextStyle(fontSize: 20),
            ),
          ),
          Flexible(
            child: ListView(
              children: [
                TextButton(
                  onPressed: () => useCase.send(TimeIsOutException()),
                  child: const Text('TimeIsOut exception'),
                ),
                const SizedBox(height: 10),
                TextButton(
                  onPressed: () => useCase.send(BadRequestException()),
                  child: const Text('BadRequest exception'),
                ),
                const SizedBox(height: 10),
                TextButton(
                  onPressed: () => useCase.send(UnauthorisedException()),
                  child: const Text('Unauthorised exception'),
                ),
                const SizedBox(height: 10),
                TextButton(
                  onPressed: () => useCase.send(FetchDataException()),
                  child: const Text('FetchData exception'),
                ),
                const SizedBox(height: 10),
                TextButton(
                  onPressed: () => useCase.send(
                    CorruptedDataException(ErrorKeys.dataCorrupted),
                  ),
                  child: const Text('CorruptedData exception'),
                ),
                const SizedBox(height: 10),
                TextButton(
                  onPressed: () => useCase.send(
                    CorruptedDataException(ErrorKeys.dataPartiallyCorrupted),
                  ),
                  child: const Text('CorruptedData(Partially) exception'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
