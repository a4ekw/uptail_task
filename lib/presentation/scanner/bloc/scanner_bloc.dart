import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task/presentation/scanner/bloc/scanner_events.dart';
import 'package:task/presentation/scanner/bloc/scanner_states.dart';

class ScannerBloc extends Bloc<ScannerBlocEvent, ScannerBlocState> {
  ScannerBloc() : super(ScannerBlocStateStart()) {
    on<ScannerBlocEventStart>((_, emit) => emit(ScannerBlocStateStart()));
    on<ScannerBlocEventPressedButton>(
      (_, emit) => emit(
        state is ScannerBlocStateStart ? ScannerBlocStatePause() : ScannerBlocStateStart(),
      ),
    );
    on<ScannerBlocEventSucceed>((_, emit) => emit(ScannerBlocStateSucceed()));
  }
}
