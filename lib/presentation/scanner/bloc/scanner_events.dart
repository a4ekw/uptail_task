abstract class ScannerBlocEvent {}

class ScannerBlocEventStart implements ScannerBlocEvent {}

class ScannerBlocEventPressedButton implements ScannerBlocEvent {}

class ScannerBlocEventSucceed implements ScannerBlocEvent {}
