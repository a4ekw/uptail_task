abstract class ScannerBlocState {}

class ScannerBlocStateStart implements ScannerBlocState {}

class ScannerBlocStatePause implements ScannerBlocState {}

class ScannerBlocStateSucceed implements ScannerBlocState {}
