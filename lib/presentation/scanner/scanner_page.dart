import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:task/presentation/scanner/bloc/scanner_bloc.dart';
import 'package:task/presentation/scanner/bloc/scanner_events.dart';
import 'package:task/presentation/scanner/bloc/scanner_states.dart';
import 'package:task/presentation/users/users_page.dart';
import 'package:task/presentation/widget/custom_app_bar_widget.dart';

class ScannerPage extends StatefulWidget {
  const ScannerPage({Key? key}) : super(key: key);

  @override
  State<ScannerPage> createState() => _ScannerPageState();
}

class _ScannerPageState extends State<ScannerPage> {
  late final ScannerBloc bloc;

  final qrViewKey = GlobalKey();

  QRViewController? qRViewController;

  bool isSucceed = false;

  @override
  void initState() {
    bloc = Get.find();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ScannerBloc, ScannerBlocState>(
        listener: onListen,
        builder: (BuildContext context, ScannerBlocState state) {
          return IgnorePointer(
            ignoring: isSucceed,
            child: Scaffold(
              appBar: CustomAppBar(title: 'scanning'.tr),
              body: Stack(
                children: [
                  QRView(
                    key: qrViewKey,
                    onQRViewCreated: _onQRViewCreated,
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    alignment: Alignment.bottomCenter,
                    child: FloatingActionButton(
                      onPressed: () => bloc.add(ScannerBlocEventPressedButton()),
                      child: Icon(
                        state is ScannerBlocStateStart ? Icons.pause : Icons.play_arrow,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _onQRViewCreated(QRViewController controller) {
    qRViewController = controller;
    controller.scannedDataStream.listen((scanData) async {
      if (!isSucceed) {
        isSucceed = true;

        controller.stopCamera();

        final snack = ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(scanData.code ?? 'NaN'),
          ),
        );
        await snack.closed.then((_) => bloc.add(ScannerBlocEventSucceed()));
      }
    });
  }

  void onListen(BuildContext context, ScannerBlocState state) {
    switch (state.runtimeType) {
      case ScannerBlocStateStart:
        qRViewController?.resumeCamera();
        break;
      case ScannerBlocStatePause:
        qRViewController?.stopCamera();
        break;
      case ScannerBlocStateSucceed:
        qRViewController?.stopCamera();

        Navigator.push(context, MaterialPageRoute(builder: (_) => const UsersPage())).then((_) {
          isSucceed = false;
          bloc.add(ScannerBlocEventStart());
        });
    }
  }

  @override
  void dispose() {
    qRViewController?.dispose();
    bloc.add(ScannerBlocEventStart());
    super.dispose();
  }
}
