import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task/domain/use_case/fetch_users_use_case.dart';
import 'package:task/presentation/users/bloc/users_events.dart';
import 'package:task/presentation/users/bloc/users_states.dart';

class UsersBloc extends Bloc<UsersBlocEvent, UsersBlocState> {
  final FetchUsersUseCase _fetchUsersUseCase;

  UsersBloc(this._fetchUsersUseCase) : super(UsersBlocStateInitial()) {
    on<UsersBlocEventFetch>(onFetch);
  }

  Future<void> onFetch(UsersBlocEventFetch event, Emitter<UsersBlocState> emit) async {
    emit(UsersBlocStateFetching());

    try {
      final list = await _fetchUsersUseCase.execute();

      emit(list.isEmpty ? UsersBlocStateEmpty() : UsersBlocStateFetched(list));
    } catch (_) {
      emit(UsersBlocStateEmpty());
    }
  }
}
