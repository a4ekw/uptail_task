import 'package:task/domain/model/user_model.dart';

abstract class UsersBlocState {}

class UsersBlocStateInitial implements UsersBlocState {}

class UsersBlocStateFetching implements UsersBlocState {}

class UsersBlocStateFetched implements UsersBlocState {
  final List<UserModel> users;

  UsersBlocStateFetched(this.users);
}

class UsersBlocStateEmpty implements UsersBlocState {}
