import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:task/presentation/users/bloc/users_bloc.dart';
import 'package:task/presentation/users/bloc/users_events.dart';
import 'package:task/presentation/users/bloc/users_states.dart';
import 'package:task/presentation/users/widget/user_tile_widget.dart';
import 'package:task/presentation/widget/custom_app_bar_widget.dart';

class UsersPage extends StatefulWidget {
  const UsersPage({Key? key}) : super(key: key);

  @override
  State<UsersPage> createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  late final UsersBloc bloc;

  @override
  void initState() {
    bloc = Get.find()..add(UsersBlocEventFetch());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'users'.tr),
      body: BlocBuilder<UsersBloc, UsersBlocState>(
          builder: (BuildContext context, UsersBlocState state) {
        switch (state.runtimeType) {
          case UsersBlocStateFetching:
            return const Center(child: CircularProgressIndicator());

          case UsersBlocStateFetched:
            final list = (state as UsersBlocStateFetched).users;
            return ListView.separated(
              itemCount: list.length,
              itemBuilder: (_, __) => const Divider(),
              separatorBuilder: (_, int index) => UserTile(list[index]),
            );

          case UsersBlocStateEmpty:
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 50.0),
                  alignment: Alignment.center,
                  child: Text('List is list_empty'.tr),
                ),
                FloatingActionButton(
                  onPressed: () => bloc.add(UsersBlocEventFetch()),
                  child: const Icon(Icons.refresh),
                ),
              ],
            );

          default:
            return const SizedBox.shrink();
        }
      }),
    );
  }
}
