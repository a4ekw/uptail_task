import 'package:flutter/material.dart';
import 'package:task/domain/model/user_model.dart';

class UserTile extends StatelessWidget {
  const UserTile(this._user, {Key? key, required}) : super(key: key);

  final UserModel _user;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(_user.toString()),
      );
}
