import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final Widget? leading;

  const CustomAppBar({
    this.title,
    this.leading,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => AppBar(
        title: title == null ? null : Text(title!),
        centerTitle: true,
        leading: leading ??
            IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: Navigator.of(context).pop,
            ),
      );

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
